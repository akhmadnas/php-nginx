FROM alpine:latest
RUN apk update && apk upgrade
RUN apk add nginx php-fpm php-mysqli supervisor
COPY web/nginx.conf /etc/nginx/nginx.conf
COPY web/supervisord.conf /etc/supervisord.conf
VOLUME ["/var/www/html/"]
COPY data/index.php /var/www/html/
RUN /usr/sbin/nginx -c /etc/nginx/nginx.conf
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]