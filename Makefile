PROJECT_ID=php-nginx-anan
LOCAL_TAG=php-app:$CI_COMMIT_BRANCH-$CI_CONCURRENT_ID
REMOTE_TAG=akhmadnas/$(PROJECT_ID):php-app-$CI_COMMIT_BRANCH-$CI_CONCURRENT_ID
CONTAINER_NAME=my_phpnginx

build:
	docker build -t $(LOCAL_TAG) .

push:
	docker tag $(LOCAL_TAG) $(REMOTE_TAG)
	docker push $(REMOTE_TAG)

deploy: 
	./containercond.sh