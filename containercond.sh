#!/bin/bash
CONTAINER_NAME='my_phpnginx'
LOCAL_TAG='php-app:I_COMMIT_BRANCH-I_CONCURRENT_ID'

if [ $( docker ps -a -f name=$CONTAINER_NAME | wc -l ) -eq 2 ]; then
    echo "Container exists" &&
    docker container stop $CONTAINER_NAME &&
	docker container rm $CONTAINER_NAME &&
    docker run -d --name=$CONTAINER_NAME --restart=unless-stopped -p 8081:80 -d --network=cc_network $LOCAL_TAG
else
    echo "Container does not exist" &&
  	docker run -d --name=$CONTAINER_NAME --restart=unless-stopped -p 8081:80 -d --network=cc_network $LOCAL_TAG
fi
